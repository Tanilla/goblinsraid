﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class RandomRewardButtonScript : MonoBehaviour 
{
	[SerializeField] private GameObject _rewardIconPrefab;
	[SerializeField] private GameObject _randomButtonsPrefab;
	[SerializeField] private UIButton _randomButton1;
	[SerializeField] private UIButton _randomButton2;
	[SerializeField] private UIButton _randomButton3;
	[SerializeField] private UIButton _randomButton4;
	[SerializeField] private UIButton _tapToContinueButton;
	[SerializeField] private GameObject _damageReward;
	[SerializeField] private GameObject _aspdReward;
	[SerializeField] private GameObject _mspdReward;
	[SerializeField] private GameObject _critChanceReward;
	[SerializeField] private GameObject _critDamageReward;
	[SerializeField] private GameObject _dodgeReward;
	[SerializeField] private GameObject _intReward;
	[SerializeField] private UILabel _rewardText;
	[SerializeField] private UILabel _statText;

	private int dmgBonus;
	private float mspdBonus;
	private float aspdBonus;
	private float mspdBonusText;
	private float aspdBonusText;
	private float critChanceBonus;
	private float critChanceBonusText;
	private float critDmgBonus;
	private float critDmgBonusText;
	private float evasionBonus;
	private float evasionBonusText;
	private float intBonus;
	private float prevStat;
	private int r;

	private EventDelegate _randomButtonEventDelegate;
	private EventDelegate _tapToContinueEventtDelegate;

	private void OnEnable()
	{
        MainGameSingletone.Instance.StateManager.StateChangedEvent += OnStateChanged;
		dmgBonus = 1;
		aspdBonus = 0.03f;
		aspdBonusText = Mathf.Round (aspdBonus * 100);
		mspdBonus = 0.02f;
		mspdBonusText = Mathf.Round (mspdBonus * 100);
		critChanceBonus = 0.01f;
		critChanceBonusText = Mathf.Round (critChanceBonus * 100);
		critDmgBonus = 0.25f;
		critDmgBonusText = Mathf.Round (critDmgBonus * 100);
		evasionBonus = 0.02f;
		evasionBonusText = Mathf.Round (evasionBonus * 100);
		intBonus = 1f;
	}

    private void OnStateChanged(GameState prev, GameState next)
    {
        if (prev != GameState.InProgress || next != GameState.NotStarted) return;
        gameObject.SetActive(false);
    }

    private void Start()
	{
		if (_randomButtonEventDelegate == null)
			_randomButtonEventDelegate = new EventDelegate(OnRandomButtonReleased);
		_randomButton1.onClick.Add(_randomButtonEventDelegate);
		_randomButton2.onClick.Add(_randomButtonEventDelegate);
		_randomButton3.onClick.Add(_randomButtonEventDelegate);
		_randomButton4.onClick.Add(_randomButtonEventDelegate);

		if (_tapToContinueEventtDelegate == null)
			_tapToContinueEventtDelegate = new EventDelegate(OnContinueButtonReleased);
		_tapToContinueButton.onClick.Add(_tapToContinueEventtDelegate);
	}
		
	public void OnRandomButtonReleased()
	{
		_rewardIconPrefab.gameObject.SetActive (true);
		_randomButtonsPrefab.gameObject.SetActive (false);

		if (MainGameSingletone.Instance.WaveManager._waveNumber % 5 == 0 && 
			MainGameSingletone.Instance.PlayerStats.PlayerDmgOutBuff < 8)
			r = 0;
		else
			Debug.Log(r = Mathf.RoundToInt(Random.Range (1f, 6f)));

		switch (r) 
		{
		default:
			_damageReward.gameObject.SetActive (true);
			_rewardText.text = string.Format (Localization.Get ("bonus_damage"), dmgBonus);
			prevStat = MainGameSingletone.Instance.PlayerStats.PlayerDmgOutBuff;
			MainGameSingletone.Instance.PlayerStats.PlayerDmgBonusReward += dmgBonus;
			MainGameSingletone.Instance.PlayerStats.UpdateStats ();
			_statText.text = string.Format (Localization.Get ("bonus_damage_change"), prevStat, 
				MainGameSingletone.Instance.PlayerStats.PlayerDmgOutBuff);
			break;
		case 1: 
			_aspdReward.gameObject.SetActive (true);
			_rewardText.text = string.Format(Localization.Get("bonus_attack_speed"),aspdBonusText);
			prevStat = 1 / MainGameSingletone.Instance.PlayerStats.PlayerAttackDelayOutBuff;
			MainGameSingletone.Instance.PlayerStats.PlayerAttackBonusReward += aspdBonus;
			MainGameSingletone.Instance.PlayerStats.UpdateStats ();
			_statText.text = string.Format (Localization.Get ("bonus_aspd_change"), string.Format("{0:0.00}",prevStat), 
				string.Format("{0:0.00}",1 / MainGameSingletone.Instance.PlayerStats.PlayerAttackDelayOutBuff));
			break;
		case 2: 
			_mspdReward.gameObject.SetActive (true);
			_rewardText.text = string.Format (Localization.Get ("bonus_move_speed"), mspdBonusText);
			prevStat = MainGameSingletone.Instance.PlayerStats.PlayerSpeedOutBuff;
			MainGameSingletone.Instance.PlayerStats.PlayerSpeedBonusReward += mspdBonus;
			MainGameSingletone.Instance.PlayerStats.UpdateStats ();
			_statText.text = string.Format (Localization.Get ("bonus_mspd_change"), 
				string.Format("{0:0.00}",prevStat),string.Format("{0:0.00}",MainGameSingletone.Instance.PlayerStats.PlayerSpeedOutBuff));
			break;
		case 3: 
			_critChanceReward.gameObject.SetActive (true);
			_rewardText.text = string.Format(Localization.Get("bonus_crit_chance"),critChanceBonusText);
			prevStat = MainGameSingletone.Instance.PlayerStats.PlayerCritChance;
			MainGameSingletone.Instance.PlayerStats.PlayerCritChance += critChanceBonus;
			MainGameSingletone.Instance.PlayerStats.UpdateStats ();
			_statText.text = string.Format (Localization.Get ("bonus_crit_chance_change"), prevStat * 100, MainGameSingletone.Instance.PlayerStats.PlayerCritChance * 100);
			break;
		case 4: 
			_critDamageReward.gameObject.SetActive (true);
			_rewardText.text = string.Format(Localization.Get("bonus_crit_damage"),critDmgBonusText);
			prevStat = MainGameSingletone.Instance.PlayerStats.PlayerCritDmg;
			MainGameSingletone.Instance.PlayerStats.PlayerCritDmg += critDmgBonus;
			MainGameSingletone.Instance.PlayerStats.UpdateStats ();
			_statText.text = string.Format (Localization.Get ("bonus_crit_damage_change"), prevStat * 100, MainGameSingletone.Instance.PlayerStats.PlayerCritDmg * 100);
			break;
		case 5:
			_dodgeReward.gameObject.SetActive (true);
			_rewardText.text = string.Format(Localization.Get("bonus_dodge"),evasionBonusText);
			prevStat = MainGameSingletone.Instance.PlayerStats.PlayerEvasion;
			MainGameSingletone.Instance.PlayerStats.PlayerEvasion += evasionBonus;
			MainGameSingletone.Instance.PlayerStats.UpdateStats ();
			_statText.text = string.Format (Localization.Get ("bonus_dodge_change"), prevStat * 100, MainGameSingletone.Instance.PlayerStats.PlayerEvasion * 100);
			break;
		case 6:
			_intReward.gameObject.SetActive (true);
			_rewardText.text = string.Format(Localization.Get("bonus_int"),intBonus);
			prevStat = MainGameSingletone.Instance.PlayerStats.PlayerIntelligence;
			MainGameSingletone.Instance.PlayerStats.PlayerIntelligence += intBonus;
			MainGameSingletone.Instance.PlayerStats.UpdateStats ();
			_statText.text = string.Format (Localization.Get ("bonus_int_change"), prevStat, MainGameSingletone.Instance.PlayerStats.PlayerIntelligence);
			break;
		}
	}

	public void OnContinueButtonReleased()
	{
		MainGameSingletone.Instance.StateManager.SetGameInProgress ();
		_randomButtonsPrefab.gameObject.SetActive (true);
		_rewardIconPrefab.gameObject.SetActive (false);

		switch (r) 
		{
		default:
			_damageReward.gameObject.SetActive (false);
			break;
		case 1:
			_aspdReward.gameObject.SetActive (false);
			break;
		case 2:
			_mspdReward.gameObject.SetActive (false);
			break;
		case 3:
			_critChanceReward.gameObject.SetActive (false);
			break;
		case 4:
			_critDamageReward.gameObject.SetActive (false);
			break;
		case 5:
			_dodgeReward.gameObject.SetActive (false);
			break;
		case 6:
			_intReward.gameObject.SetActive (false);
			break;
		}

		gameObject.SetActive (false);
	}
}
