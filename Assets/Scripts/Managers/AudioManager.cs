﻿using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour, IManager
{
    [SerializeField] private AudioMixer _mixer;
    [SerializeField] private AudioSource _main;
    [SerializeField] private AudioSource _ambient;
    [SerializeField] private AudioSource _battle;
    [SerializeField] private AudioSource _calm;

    public void Init()
    {
        _main.Play();
        //MainGameSingletone.Instance.StateManager.StateChangedEvent += OnGameStateChange;
    }

    public void Uninit()
    {
    }

    public void MuteSound()
    {
        _mixer.SetFloat("SoundsVolume", -80);
    }

    public void MuteMusic()
    {
        _mixer.SetFloat("MusicVolume", -80);
    }

    public void UnmuteSound()
    {
        _mixer.SetFloat("SoundsVolume", 0);
    }

    public void UnmuteMusic()
    {
        _mixer.SetFloat("MusicVolume", 0);
    }

    private void OnGameStateChange(GameState prev, GameState curr)
    {
        if (curr == GameState.InProgress && prev == GameState.NotStarted)
        {
            _main.Stop();
            _ambient.Play();
            _calm.Play();
        }
        else if (curr == GameState.NotStarted && prev == GameState.InProgress)
        {
            _main.Play();
            _ambient.Stop();
            _calm.Stop();
        }
    }
}
