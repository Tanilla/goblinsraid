﻿using System.Collections;
using DarkTonic.MasterAudio;
using UnityEngine;

public class WaveMessage : MonoBehaviour
{
    [SerializeField] private UILabel _waveText;

    private void Start ()
    {
        _waveText.enabled = false;
    }

    public IEnumerator Countdown()
    {
        _waveText.enabled = true;

        for (var i = 5; i > 0; i--)
        {
			_waveText.text = i.ToString ();
            MasterAudio.PlaySoundAndForget("countdown");
			yield return new WaitForSeconds (1f);
        }
        _waveText.enabled = false;
    }

    public IEnumerator ShowMessage(string key, float time, params object[] values)
    {
        _waveText.enabled = true;
        _waveText.text = string.Format(Localization.Get(key), values);
        yield return new WaitForSecondsCustom(time);
        _waveText.enabled = false;
    }
}
