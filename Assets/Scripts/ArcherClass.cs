﻿using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;

public class ArcherClass : PlayerController
{
    protected override void Start()
    {
        base.Start();
        _animator.runtimeAnimatorController =
            Resources.Load<RuntimeAnimatorController>("Animations/PlayerAnimations/ArcherAnimations/ArcherAnimController");
        BowShootSound = "bow.shoot";
        DeadSound = "character.death";
        StepSound = "step.character";
        SuperSkillSound = "ultimate";
        SkillSound = "trap.active";
        delaySE = 1.25f;
    }

    protected override void Skill()
    {
        if (MainGameSingletone.Instance.PlayerStats.PlayerSkillPoints < PlayerStats.SkillCost || _isCasting) return;
        MainGameSingletone.Instance.PlayerStats.TotalSkillUse++;
        MainGameSingletone.Instance.PlayerStats.PlayerSkillPoints -= PlayerStats.SkillCost;
        MasterAudio.PlaySoundAndForget(SkillSound);
        var res = Resources.Load("Prefabs/TrapPrefab") as GameObject;
        var go = Instantiate(res);
        if (go == null) return;
        go.transform.position = new Vector3(transform.position.x, transform.position.y + 1f);
    }

    protected override void SuperSkill()
    {
        if (MainGameSingletone.Instance.PlayerStats.PlayerSkillPoints < PlayerStats.SuperSkillCost || _isCasting)
            return;
        MainGameSingletone.Instance.PlayerStats.TotalSuperSkillUse++;
        _superEffect.SetActive(true);
        _isBlinking = true;
        _animator.SetTrigger("Attack");
        _animator.speed = 0.5f;
        _isCasting = true;
    }

    protected override void SuperEffect()
    {
        MasterAudio.PlaySoundAndForget(SuperSkillSound);
        MainGameSingletone.Instance.PlayerStats.PlayerSkillPoints -= PlayerStats.SuperSkillCost;
        for (var i = 35f; i >= -35f; i -= 17.5f) {
            var projectile = Resources.Load(ProjectilePrefabPath);
            var go = Instantiate(projectile) as GameObject;
            if (go == null) continue;
            var arrow = go.gameObject.GetComponent<PlayerProjectileScript>();
            arrow.SuperArrow = true;
            arrow.Dmg = 60;
            arrow.transform.Rotate(0, 0, i);
            arrow.transform.position = new Vector3(transform.position.x + 0.2f, transform.position.y);
            arrow.transform.localScale = Vector3.one;
        }
        _superEffect.SetActive(false);
        _isCasting = false;
        _isBlinking = false;
    }
}
