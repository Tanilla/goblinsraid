﻿using UnityEngine;
using System.Collections;

public class TeleportState : StateMachineBehaviour
{
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.transform.position = new Vector3(Random.Range(-5f, 5f), Random.Range(animator.transform.position.y, animator.transform.position.y + 0.9f));
    }
}
