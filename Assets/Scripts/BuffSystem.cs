using System;
using System.Linq;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;


public class BuffSystem : MonoBehaviour, IManager
{
    private readonly Queue<string> _buffs = new Queue<string>();
	private float asBonus;
	private float asBonusText;
	private float msBonus;
	private float msBonusText;

    public int BuffCount
    {
        get { return _buffs.Count; }
    }

    private readonly List<Coroutine> _coroutines = new List<Coroutine>();
    private const int MaxBuffsCount = 3;
    private const float BuffDuration = 30f;
    private const float InvincibilityDuration = 15f;

    public event Action<Queue<string>> BuffsChanged;

    private float _counter = InvincibilityDuration;

    public void AddBuff(Buff buff)
    {
        if (buff.Type != Buff.BuffType.Protection)
        {
            if (_buffs.Count < MaxBuffsCount && _buffs.Count(s => s == buff.HUDSprite) < 2)
            {
                _buffs.Enqueue(buff.HUDSprite);
                _coroutines.Add(StartCoroutine(ApplyBuff(buff.Type)));
            }
            else
                MainGameSingletone.Instance.PlayerStats.AddPointsToScore(100);

            if (BuffsChanged != null)
                BuffsChanged(_buffs);
        }
        else if (MainGameSingletone.Instance.PlayerStats.IsInvincible)
            _counter = InvincibilityDuration;
        else
            _coroutines.Add(StartCoroutine(ApplyInvincibility()));
    }

    private IEnumerator ApplyInvincibility()
    {
        var type = Buff.BuffType.Protection;
        MainGameSingletone.Instance.PlayerStats.IsInvincible = true;
        while (_counter > 0)
        {
            var wait = new WaitForSeconds(1f);
            yield return wait;
            _counter--;

			if (_counter < 2f) 
			{
				MainGameSingletone.Instance._playerController._bubble.GetComponent<Animator>().SetTrigger ("StartEnd");
			}
        }
        NullifyBuff(type);
    }

    private void RemoveBuff()
    {
        _buffs.Dequeue();
        if (BuffsChanged != null)
            BuffsChanged(_buffs);
    }

    public void ClearBuffs()
    {
        MainGameSingletone.Instance.PlayerStats.RecalculateStats();
        foreach (var coroutine in _coroutines)
            StopCoroutine(coroutine);
        _buffs.Clear();
        if (BuffsChanged != null)
            BuffsChanged(_buffs);
    }

    private IEnumerator ApplyBuff(Buff.BuffType type)
    {
		asBonus = 0.18f;
		asBonusText = asBonus * 100;
		msBonus = 0.15f;
		msBonusText = msBonus * 100;
        switch (type)
        {
            case Buff.BuffType.Ammunition:
                MainGameSingletone.Instance.PlayerStats.PlayerProjectileCount++;
                MainGameSingletone.Instance._playerController._hudText.Add
                    (Localization.Get("buff_ammunition_add"), Color.green, 0f);
                break;
            case Buff.BuffType.Attack:
                MainGameSingletone.Instance.PlayerStats.PlayerAttackBonusBuff += asBonus;
                MainGameSingletone.Instance._playerController._hudText.Add
                    (string.Format(Localization.Get("buff_attack_add"), asBonusText), Color.green, 0f);
                break;
            case Buff.BuffType.Damage:
                MainGameSingletone.Instance.PlayerStats.PlayerDmgBonusBuff++;
                MainGameSingletone.Instance._playerController._hudText.Add
                    (string.Format(Localization.Get("buff_damage_add"), 1), Color.green, 0f);
                break;
            case Buff.BuffType.Speed:
                MainGameSingletone.Instance.PlayerStats.PlayerSpeedBonusBuff += msBonus;
                MainGameSingletone.Instance._playerController._hudText.Add
                    (string.Format(Localization.Get("buff_speed_add"), msBonusText), Color.green, 0f);
                break;
        }
        MainGameSingletone.Instance.PlayerStats.UpdateStats ();
        yield return new WaitForSeconds(BuffDuration);
        NullifyBuff(type);
        RemoveBuff();
    }

    private void NullifyBuff(Buff.BuffType type)
    {
        switch (type)
        {
            case Buff.BuffType.Ammunition:
                MainGameSingletone.Instance.PlayerStats.PlayerProjectileCount--;
                MainGameSingletone.Instance._playerController._hudText.Add
                    (Localization.Get("buff_ammunition_remove"), Color.gray, 0f);
                break;
            case Buff.BuffType.Attack:
                MainGameSingletone.Instance.PlayerStats.PlayerAttackBonusBuff -= asBonus;
                MainGameSingletone.Instance._playerController._hudText.Add
                    (string.Format(Localization.Get("buff_attack_remove"), asBonusText), Color.gray, 0f);
                break;
            case Buff.BuffType.Damage:
                MainGameSingletone.Instance.PlayerStats.PlayerDmgBonusBuff--;
                MainGameSingletone.Instance._playerController._hudText.Add
                    (string.Format(Localization.Get("buff_damage_remove"), 1), Color.gray, 0f);
                break;
            case Buff.BuffType.Protection:
                _counter = InvincibilityDuration;
                MainGameSingletone.Instance.PlayerStats.IsInvincible = false;
                break;
            case Buff.BuffType.Speed:
                MainGameSingletone.Instance.PlayerStats.PlayerSpeedBonusBuff -= msBonus;
                MainGameSingletone.Instance._playerController._hudText.Add
                    (string.Format(Localization.Get("buff_speed_remove"), msBonusText), Color.gray, 0f);
                break;
        }
        MasterAudio.PlaySoundAndForget ("buff.down");
		MainGameSingletone.Instance.PlayerStats.UpdateStats ();
    }

    public void Init()
    {
        MainGameSingletone.Instance.StateManager.StateChangedEvent += OnStateChanged;
    }

    private void OnStateChanged(GameState prevState, GameState newState)
    {
        if (prevState == GameState.NotStarted && newState == GameState.InProgress)
            ClearBuffs();
    }

    public void Uninit()
    {
        MainGameSingletone.Instance.StateManager.StateChangedEvent -= OnStateChanged;
    }
}
