﻿using UnityEngine;

public class DeadState : StateMachineBehaviour
{
    public override void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
    {
        animator.SetInteger("index", Random.Range(0, 5));
    }

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponentInParent<EnemyClass>().CancelInvoke();
        animator.GetComponentInParent<SpriteRenderer>().sortingOrder = 3;
        animator.transform.position = new Vector3(animator.transform.position.x, animator.transform.position.y - 0.35f);
        MainGameSingletone.Instance.WaveManager.DeadEnemies++;
        animator.GetComponentInParent<EnemyClass>().Death();
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.gameObject.SetActive(false);
    }
}
