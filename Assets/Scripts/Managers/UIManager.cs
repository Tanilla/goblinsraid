﻿using UnityEngine;

public class UIManager : MonoBehaviour, IManager
{
    public ExitButtonScript ExitButton;
	public RandomRewardButtonScript ChooseReward;
    public SkillProgressBar SkillBar;
	public VictoryFormScript VictoryForm;
	public DefeatFormScript DefeatForm;
	public StatisticScreenScript StatisticScreen;
    public AttackButtonScript AttackButton;
    public WaveProgressAndHealthPanelScript WaveAndHealthPanel;
    public ScoreWidgetScript ScoreWidget;
    public MovementWidgetScript MovementWidget;
    public WaveMessage MessageWidget;
	[SerializeField] public GameObject BattleUI;
    public GameLobbyScript GameLobbyWidget;

    public void Init()
    {
        MainGameSingletone.Instance.StateManager.StateChangedEvent += OnStateChanged;
        GameLobbyWidget.Init();
        ScoreWidget.Init();
        WaveAndHealthPanel.Init();
        SkillBar.Init();
    }

    private void OnStateChanged(GameState prevState, GameState newState)
    {
        if ((prevState == GameState.InProgress || prevState == GameState.Pause) && newState == GameState.NotStarted)
        {
			GameLobbyWidget.gameObject.SetActive(false);
            ExitButton.gameObject.SetActive(false);
			VictoryForm.gameObject.SetActive (false);
			DefeatForm.gameObject.SetActive (false);
			StatisticScreen.gameObject.SetActive (true);
            SkillBar.gameObject.SetActive(false);
            AttackButton.gameObject.SetActive(false);
            WaveAndHealthPanel.gameObject.SetActive(false);
            ScoreWidget.gameObject.SetActive(false);
            MovementWidget.gameObject.SetActive(false);
            MessageWidget.gameObject.SetActive(false);
        }
        else if (prevState == GameState.NotStarted && newState == GameState.InProgress)
        {
            GameLobbyWidget.gameObject.SetActive(false);
            ExitButton.gameObject.SetActive(true);
            SkillBar.gameObject.SetActive(true);
            AttackButton.gameObject.SetActive(true);
            WaveAndHealthPanel.gameObject.SetActive(true);
            ScoreWidget.gameObject.SetActive(true);
            MovementWidget.gameObject.SetActive(true);
            MessageWidget.gameObject.SetActive(true);
        }
    }

    public void Uninit()
    {
        GameLobbyWidget.Uninit();
        ScoreWidget.Uninit();
        WaveAndHealthPanel.Uninit();
        SkillBar.Uninit();
        MainGameSingletone.Instance.StateManager.StateChangedEvent -= OnStateChanged;
    }
}
