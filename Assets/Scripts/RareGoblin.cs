using System.Collections;
using DarkTonic.MasterAudio;
using UnityEngine;

public class RareGoblin : EnemyClass
{
	private float _speedStage1;
	private float _speedStage2;
	private float _hpTreshold1;
	private float _hpTreshold2;
	private bool _stage1;
	private bool _stage2;

    protected override void OnEnable()
    {
        base.OnEnable();
        RuntimeControllerPath = "Animations/RareAnimations/RareAnimController";
        Animator.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>(RuntimeControllerPath);
        DropChance = 1f;
        DropableBuffs = new[] {Buff.BuffType.Score};
	    IsReflecting = true;
		HP = 54;
		BaseSpeed = 0.19f;
		_hpTreshold1 = Mathf.RoundToInt(HP * 0.6f);
		_hpTreshold2 = Mathf.RoundToInt(HP * 0.3f);
	    PointsForKill = 45;
	    PenaltyPoints = 45;
		PointsForHit = 3;
		Speed = BaseSpeed;
		_speedStage1 = BaseSpeed * 1.5f;
		_speedStage2 = BaseSpeed * 2.3f;
		StepSound = "rare.move";
    }

	private void Update()
	{
		if (HP > 0)
			ChangeStage ();
	}

	private void ChangeStage()
	{
	    if (IsDead) return;
		if (HP <= _hpTreshold1 && !_stage1)
	    {
	        MasterAudio.PlaySoundAndForget("rare.crash");
	        Animator.SetTrigger("StageB");
	        Speed = _speedStage1;
	        _stage1 = true;
	        IsReflecting = false;
	    }
		else if (HP <= _hpTreshold2 && !_stage2)
	    {
	        MasterAudio.PlaySoundAndForget("rare.crash");
	        Animator.SetTrigger("StageC");
	        Speed = _speedStage2;
	        _stage2 = true;
	    }
	}

	public override void Death()
	{
		base.Death();
		MainGameSingletone.Instance.WaveManager.IsRareExists = false;
	}
}