﻿using UnityEngine;

public class HUDTextManager : MonoBehaviour
{
    public HUDText HudText { get; private set; }

    private UIFollowTarget _followTarget;

    public void Awake()
    {
        var hud = Instantiate(Resources.Load<GameObject>("Prefabs/HUDText"));
        hud.transform.SetParent(UICamera.mainCamera.transform);
        HudText = hud.GetComponent<HUDText>();
        _followTarget = hud.GetComponent<UIFollowTarget>();
        _followTarget.target = transform;
    }
}
