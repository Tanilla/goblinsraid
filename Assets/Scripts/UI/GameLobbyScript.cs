﻿using DarkTonic.MasterAudio;
using UnityEngine;

public class GameLobbyScript : MonoBehaviour
{
    [SerializeField] private UIButton _playButton;
	[SerializeField] private UIButton _backButton;
	[SerializeField] private UIButton _chooseButton;
    [SerializeField] private UIToggle _soundButton;
    [SerializeField] private UIToggle _musicButton;
    [SerializeField] private UILabel _bestScore;
    [SerializeField] private UILabel _lastScore;
	[SerializeField] public GameObject _mainMenu;
	[SerializeField] public GameObject _charChoose;

    private EventDelegate _playButtonEventDelegate;
	private EventDelegate _backButtonEventDelegate;
	private EventDelegate _chooseButtonEventDelegate;
    private EventDelegate _soundMuteButtonDelegate;
    private EventDelegate _musicMuteButtonDelegate;

    private void OnEnable()
    {
        MasterAudio.FireCustomEvent("PlayMainTheme", transform.position);

        if (_playButtonEventDelegate == null)
            _playButtonEventDelegate = new EventDelegate(OnPlayButtonReleased);
        _playButton.onClick.Add(_playButtonEventDelegate);

        if (_soundMuteButtonDelegate == null)
            _soundMuteButtonDelegate = new EventDelegate(OnSoundButtonPress);
        _soundButton.onChange.Add(_soundMuteButtonDelegate);

        if (_musicMuteButtonDelegate == null)
            _musicMuteButtonDelegate = new EventDelegate(OnMusicButtonPress);
        _musicButton.onChange.Add(_musicMuteButtonDelegate);

		if (_backButtonEventDelegate == null)
			_backButtonEventDelegate = new EventDelegate(OnBackButtonReleased);
		_backButton.onClick.Add(_backButtonEventDelegate);

		if (_chooseButtonEventDelegate == null)
			_chooseButtonEventDelegate = new EventDelegate(OnChooseButtonReleased);
		_chooseButton.onClick.Add(_chooseButtonEventDelegate);
        _soundButton.Set(PlayerPrefs.GetInt("SoundToggle") == 1);
        _musicButton.Set(PlayerPrefs.GetInt("MusicToggle") == 1);
    }

    private void Awake()
    {
        MainGameSingletone.Instance.Init();
        if (Application.systemLanguage == SystemLanguage.Belarusian ||
            Application.systemLanguage == SystemLanguage.Russian ||
            Application.systemLanguage == SystemLanguage.Ukrainian)
            Localization.language = "RUS";
        else {
            Localization.language = "ENG";
        }
    }

    private void OnDisable()
    {
        _playButton.onClick.Remove(_playButtonEventDelegate);
		_chooseButton.onClick.Remove(_chooseButtonEventDelegate);
        _musicButton.onChange.Remove(_musicMuteButtonDelegate);
        _soundButton.onChange.Remove(_soundMuteButtonDelegate);
    }

    private void OnSoundButtonPress()
    {
        var status = _soundButton.value ? 1 : 0;
        PlayerPrefs.SetInt("SoundToggle", status);
        MasterAudio.FireCustomEvent(_soundButton.value ? "SoundMute" : "SoundUnmute", transform.position);
    }

    private void OnMusicButtonPress()
    {
        var status = _musicButton.value ? 1 : 0;
        PlayerPrefs.SetInt("MusicToggle", status);
        MasterAudio.FireCustomEvent(_musicButton.value ? "MusicMute" : "MusicUnmute", transform.position);
    }

    private void OnPlayButtonReleased()
    {
		//_mainMenu.gameObject.SetActive (false);
		//_charChoose.gameObject.SetActive (true);
		MainGameSingletone.Instance.UIManager.BattleUI.gameObject.SetActive(true);
		MainGameSingletone.Instance.StateManager.SetGameInProgress();
		MasterAudio.FireCustomEvent("PlayBattleTheme", transform.position);
		gameObject.SetActive (false);
    }

	private void OnBackButtonReleased()
	{
		_mainMenu.gameObject.SetActive (true);
		_charChoose.gameObject.SetActive (false);
	}

	private void OnChooseButtonReleased()
	{
		_charChoose.gameObject.SetActive (false);
		_mainMenu.gameObject.SetActive (true);
		MainGameSingletone.Instance.StateManager.SetGameInProgress();
		MainGameSingletone.Instance.UIManager.BattleUI.gameObject.SetActive(true);
		MasterAudio.FireCustomEvent("PlayBattleTheme", transform.position);
		gameObject.SetActive (false);
	}

    private void OnBestScoreChange(int score)
    {
        _bestScore.text = score.ToString();
    }

    private void OnLastScoreChange(int score)
    {
        _lastScore.text = score.ToString();
    }

    public void Init()
    {
        MainGameSingletone.Instance.PlayerStats.BestScoreChange += OnBestScoreChange;
        MainGameSingletone.Instance.PlayerStats.LastScoreChange += OnLastScoreChange;
    }

    public void Uninit()
    {
        MainGameSingletone.Instance.PlayerStats.BestScoreChange -= OnBestScoreChange;
        MainGameSingletone.Instance.PlayerStats.LastScoreChange -= OnLastScoreChange;
    }
}
