﻿using UnityEngine;

public class AttackButtonScript : MonoBehaviour
{
	public static bool ButtonPress { get; private set; }

    private void OnEnable()
    {
        ButtonPress = false;
    }

	public void OnButtonPressed()
    {
		ButtonPress = true;
	}

    public void OnRelease()
    {
        ButtonPress = false;
    }
}
