﻿using UnityEngine;
using System.Collections;

public class SuperEffectScript : StateMachineBehaviour 
{
	public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		animator.GetComponentInParent<PlayerController>().Invoke("SuperEffect", animator.GetComponentInParent<PlayerController>().delaySE);
	}
}
