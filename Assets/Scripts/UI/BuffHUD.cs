﻿using UnityEngine;
using System.Collections.Generic;

public class BuffHUD : MonoBehaviour
{
    [SerializeField] private UISprite[] _sprites;

    public void Init()
    {
        MainGameSingletone.Instance.BuffSystem.BuffsChanged += OnBuffHUDChange;
    }

    private void OnBuffHUDChange(Queue<string> buffs)
    {
        var tmp = buffs.ToArray();
        for (var i = 0; i < tmp.Length; i++)
            _sprites[i].spriteName = tmp[i];
        for (var i = buffs.Count; i < _sprites.Length; i++)
            _sprites[i].spriteName = "baf";
    }
}
