﻿using System;
using System.Collections;
using UnityEngine;

public class PlayerStats : IManager
{
    public event Action<int> ScoreChanged;
    public event Action<int> HealthPointsChanged;
    public event Action<float> SkillPointsChanged;
    public event Action<bool> InvincibilityChanged;
    public event Action<int> BestScoreChange;
    public event Action<int> LastScoreChange;

    private int _score;
	public int Score
    {
        get { return _score; }
        set
        {
            _score = value;
            if (MainGameSingletone.Instance.StateManager.CurrentState != GameState.NotStarted)
                SaveScore();
            if (ScoreChanged != null)
                ScoreChanged(_score);
        }
    }

    private int _playerHealthPoints;
    public int PlayerHealthPoints
    {
        get { return _playerHealthPoints; }
        private set
        {
            _playerHealthPoints = value;
            if (HealthPointsChanged != null)
                HealthPointsChanged(_playerHealthPoints);
        }
    }

    private float _playerSkillPoints;
    public float PlayerSkillPoints
    {
        get { return _playerSkillPoints; }
        set
        {
            _playerSkillPoints = value;
            if (SkillPointsChanged != null)
                SkillPointsChanged(_playerSkillPoints);
        }
    }

    public int _bestScore;
    private int BestScore
    {
        get { return _bestScore; }
        set
        {
            _bestScore = value;
            if (BestScoreChange != null)
                BestScoreChange(_bestScore);
        }
    }

    private int _lastScore;
    private int LastScore
    {
        get { return _lastScore; }
        set
        {
            _lastScore = value;
            if (LastScoreChange != null)
                LastScoreChange(_lastScore);
        }
    }

	public float PlayerDmg { get; set; }
	public float PlayerDmgOutBuff { get; set; }
	public float PlayerCritChance { get; set; }
	public float PlayerCritDmg { get; set; }
	public float PlayerSpeed { get; set; }
	public float PlayerSpeedOutBuff { get; set; }
	public float PlayerAttackDelay { get; set; }
	public float PlayerAttackDelayOutBuff { get; set; }
	public float PlayerEvasion { get; set; }
	public float PlayerIntelligence { get; set; }
	public float PlayerDmgBonusBuff;
	public float PlayerDmgBonusReward;
	public float PlayerSpeedBonusBuff;
	public float PlayerSpeedBonusReward;
	public float PlayerAttackBonusBuff;
	public float PlayerAttackBonusReward;
	public int PlayerProjectileCount { get; set; }
	public static float SkillCost;
	public static float SuperSkillCost;
    private bool _isInvincible;
	public bool IsBlinking;

	//statisitcs
	public int TotalKills;
	public float TotalMaxCombo;
	public int TotalWaveNumber;
	public int TotalDeaths;
	public float TotalAccuracy;
	public float CurrentShotsAllNumber; 
	public float CurrentShotsInNumber; 
	public int TotalSkillUse;
	public int TotalSuperSkillUse;

    public bool IsInvincible
    {
        get { return _isInvincible; }
        set
        {
            _isInvincible = value;
            if (InvincibilityChanged != null)
                InvincibilityChanged(value);
        }
    }

    public void RecalculateStats()
    {
		PlayerDmgBonusBuff = 0;
		PlayerDmg = MainGameSingletone.Instance.ArrowDamage + PlayerDmgBonusBuff + PlayerDmgBonusReward;
		PlayerSpeedBonusBuff = 0;
		PlayerSpeed = MainGameSingletone.Instance.PlayerMovementSpeed * ((1 + PlayerSpeedBonusBuff + PlayerSpeedBonusReward));
		PlayerAttackBonusBuff = 0;
		PlayerAttackDelay = MainGameSingletone.Instance.PlayerAttackDelay * ((1 - (PlayerAttackBonusBuff + PlayerAttackBonusReward)));
		PlayerProjectileCount = 1;
        IsInvincible = false;
    }

    private void ResetStats()
    {
        BestScore = PlayerPrefs.GetInt("BestScore");
        LastScore = PlayerPrefs.GetInt("LastScore");
		SkillCost = 120f;
		SuperSkillCost = 224;
		PlayerDmgBonusBuff = 0;
		PlayerDmgBonusReward = 0;
		PlayerDmg = MainGameSingletone.Instance.ArrowDamage + PlayerDmgBonusBuff + PlayerDmgBonusReward;
		PlayerDmgOutBuff = MainGameSingletone.Instance.ArrowDamage + PlayerDmgBonusReward;
		PlayerSpeedBonusBuff = 0;
		PlayerSpeedBonusReward = 0;
		PlayerSpeedOutBuff = MainGameSingletone.Instance.PlayerMovementSpeed * ((1 + PlayerSpeedBonusReward));
		PlayerSpeed = MainGameSingletone.Instance.PlayerMovementSpeed * ((1 + PlayerSpeedBonusBuff + PlayerSpeedBonusReward));
		PlayerAttackBonusBuff = 0;
		PlayerAttackBonusReward = 0;
		PlayerAttackDelay = MainGameSingletone.Instance.PlayerAttackDelay * ((1 - (PlayerAttackBonusBuff + PlayerAttackBonusReward)));
		PlayerAttackDelayOutBuff =  MainGameSingletone.Instance.PlayerAttackDelay * ((1 - PlayerAttackBonusReward));
        PlayerProjectileCount = 1;
		PlayerCritChance = 0.15f;
		PlayerCritDmg = 1.5f;
		PlayerEvasion = 0.18f;
		PlayerIntelligence = 1f;
		PlayerHealthPoints = 10;
        PlayerSkillPoints = 0;
        Score = 0;
        IsInvincible = false;
		IsBlinking = false;
		TotalKills = 0;
		TotalMaxCombo = 0f;
		TotalWaveNumber = 1;
		TotalDeaths = 0;
		TotalAccuracy = 0f;
		CurrentShotsAllNumber = 0;
		CurrentShotsInNumber = 0;
		TotalSkillUse = 0;
		TotalSuperSkillUse = 0;
    }

	public void UpdateStats()
	{
		PlayerDmg = MainGameSingletone.Instance.ArrowDamage + PlayerDmgBonusBuff + PlayerDmgBonusReward;
		PlayerDmgOutBuff = MainGameSingletone.Instance.ArrowDamage + PlayerDmgBonusReward;
		PlayerSpeed = MainGameSingletone.Instance.PlayerMovementSpeed * ((1 + PlayerSpeedBonusBuff + PlayerSpeedBonusReward));
		PlayerSpeedOutBuff = MainGameSingletone.Instance.PlayerMovementSpeed * ((1 + PlayerSpeedBonusReward));
		PlayerAttackDelay = MainGameSingletone.Instance.PlayerAttackDelay * ((1 - (PlayerAttackBonusBuff + PlayerAttackBonusReward)));
		PlayerAttackDelayOutBuff =  MainGameSingletone.Instance.PlayerAttackDelay * ((1 - PlayerAttackBonusReward));
	}

    private void SaveScore()
    {
        PlayerPrefs.SetInt("LastScore", Score);
        if (Score > PlayerPrefs.GetInt("BestScore"))
            PlayerPrefs.SetInt("BestScore", Score);
    }

    public void AddSkillPoints(float points)
    {
		var skillPoints = PlayerSkillPoints + points; //+ (PlayerSkillPoints < 30 ? points : points * 1.5f);
		PlayerSkillPoints = Mathf.Clamp(skillPoints, 0f, SuperSkillCost);
    }

    public void AddPointsToScore(int points)
    {
        Score += points;
    }

    public void Init()
    {
        ResetStats();
        MainGameSingletone.Instance.StateManager.StateChangedEvent += OnStateChanged;
    }

    private void OnStateChanged(GameState prevState, GameState newState)
    {
        if (newState == GameState.NotStarted)
        {
            ResetStats();
        }
        else if (prevState == GameState.NotStarted && newState == GameState.InProgress)
        {
            ResetStats();
        }
    }

    public void Uninit()
    {
        MainGameSingletone.Instance.StateManager.StateChangedEvent -= OnStateChanged;
    }

    public void AddHealthPoints(int count)
    {
        if (PlayerHealthPoints > 10)
            Score += 100;
        else
            PlayerHealthPoints += count;
    }

    public void RemoveHealthPoints(int count)
    {
		TotalDeaths++;
		PlayerHealthPoints -= count;
        if (PlayerHealthPoints > 0) return;
		Defeat ();
    }

	private void Defeat()
	{
		MainGameSingletone.Instance.UIManager.StatisticScreen.GameResult = 2;
		MainGameSingletone.Instance.UIManager.DefeatForm.gameObject.SetActive (true);
		MainGameSingletone.Instance.StateManager.SetGameOnPause();
	}
}