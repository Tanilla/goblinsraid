﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class EnemyPoolManager : MonoBehaviour, IManager
{
    [SerializeField] private GameObject _poolObject;
    [SerializeField] private bool _canGrow;
    private readonly List<GameObject> _pool = new List<GameObject>();

    public GameObject PoolObject
    {
        get
        {
            GameObject obj;
            try
            {
                obj = _pool.FirstOrDefault(x => x.activeInHierarchy == false && x.gameObject.GetComponent<EnemyClass>() == null);
            }
            catch (InvalidOperationException)
            {
                if (!_canGrow) return null;
                obj = Instantiate(_poolObject);
                _pool.Add(obj);
            }
            return obj;
        }
    }

    public void Init()
    {
        for (var i = 0; i < 50; i++)
        {
            var obj = Instantiate(_poolObject);
            obj.transform.SetParent(transform);
            _pool.Add(obj);
        }
    }

    public void Uninit()
    {}
}
