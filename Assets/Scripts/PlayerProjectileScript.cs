﻿using UnityEngine;

public class PlayerProjectileScript : MonoBehaviour
{
	public float Dmg { get; set; }
	public float Range { private get; set; }
	private float Speed;
	public float SpeedModifer;
	private Animator _animator;

	public bool SuperArrow;

	private void Start()
	{
		_animator = GetComponent<Animator>();
	}

	private void Awake()
	{
		MainGameSingletone.Instance.StateManager.StateChangedEvent += OnStateChange;
		Dmg = MainGameSingletone.Instance.PlayerStats.PlayerDmg;
		Range = 3.5f;
		Speed = MainGameSingletone.Instance.PlayerProjectileMovementSpeed;
		SpeedModifer = 1f;
	}

	private void Update ()
	{
		if (SuperArrow)
			_animator.SetTrigger ("SuperArrow");

		if (transform.localPosition.y < Range)
			transform.position += transform.up * Time.deltaTime * Speed * SpeedModifer;
		else
			Destroy(gameObject);
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        MainGameSingletone.Instance.StateManager.StateChangedEvent -= OnStateChange;
    }

    private void OnStateChange(GameState prev, GameState next)
    {
        if ( next != GameState.NotStarted) return;
        StopAllCoroutines();
        Destroy(gameObject);
    }
}
