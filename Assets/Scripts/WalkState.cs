﻿using UnityEngine;
using DarkTonic.MasterAudio;

public class WalkState : StateMachineBehaviour
{
	private float _speed;
	private float _speedModifer;
	private int _penalty;
	private string _stepSound;

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
		_speed = animator.GetComponentInParent<EnemyClass> ().Speed;
		_speedModifer = animator.GetComponentInParent<EnemyClass> ().SpeedModifer;
		_stepSound = animator.GetComponentInParent<EnemyClass> ().StepSound;
		_penalty = animator.GetComponentInParent<EnemyClass> ().Penalty;

		if (animator.transform.position.y > -1.5f) 
		{
			animator.transform.position -= animator.transform.up * Time.deltaTime * _speed * _speedModifer;
			animator.GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt(1000 - 100 * animator.transform.position.y);
			MasterAudio.PlaySoundAndForget (_stepSound);
		}
        else
        {
			MainGameSingletone.Instance.PlayerStats.AddPointsToScore(-_penalty);
            MainGameSingletone.Instance.PlayerStats.RemoveHealthPoints(1);
			animator.GetComponentInParent<EnemyClass> ()._hudText.Add
					(string.Format("-{0}", _penalty), Color.white, 0f);
            animator.SetBool("Dead", true);
        }
    }
}