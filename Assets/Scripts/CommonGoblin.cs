using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;

public class CommonGoblin : EnemyClass
{
	private float _attackDelay;

    protected override void OnEnable()
    {
        base.OnEnable();
        //DropChance = 1f;
        //DropableBuffs = new[] { Buff.BuffType.Protection };
        ProjectilePrefabPath = "Prefabs/CommonProjectilePrefab";
        RuntimeControllerPath = "Animations/CommonAnimations/CommonAnimController";
        Animator.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>(RuntimeControllerPath);
		HP = 4;
		BaseSpeed = 0.4f;
        PointsForKill = 8;
        PenaltyPoints = 8;
        PointsForHit = 1;
        InvokeDelay = 0.6f;
        Speed = BaseSpeed;
		ShootSound = "common.spear";
		StepSound = "step.goblin";
    }

	protected IEnumerator OnBecameVisible ()
	{
		while (true)
		{

			if (Timer) {
				_attackDelay = 3 + Mathf.Round (Random.Range (0, 10));
				Timer = false;
				yield return new WaitForSeconds (_attackDelay);
				Timer = true;

				if (transform.localPosition.y > 0.3f) 
				{
					Animator.SetTrigger ("Attack");
					attackNow = true;
				}
			} 
			else
				break;
        }
    }
}
