using System.Collections;
using UnityEngine;
using DarkTonic.MasterAudio;

public class Buff : MonoBehaviour
{
    public enum BuffType
    {
        Ammunition,
        Attack,
        Damage,
        Protection,
        Speed,
        Score
    }
    public BuffType Type { get; set; }
    public string HUDSprite { get; private set; }
	private string FlySound;
	private string PicUpSound;
	private bool live;
	private bool _isBlinking;
	private SpriteRenderer _rend;

    private void Start()
    {
        MainGameSingletone.Instance.StateManager.StateChangedEvent += OnStateChange;
        GetComponent<SpriteRenderer>().sortingOrder = 5;
		_rend = GetComponent<SpriteRenderer>();
		FlySound = "buff.fly";
		PicUpSound = "buff.take";
		live = true;

        switch (Type)
        {
            case BuffType.Ammunition:
                GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Atlas/Sprites/buffs/ammunition");
                HUDSprite = "baf_5";
                break;
            case BuffType.Attack:
                GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Atlas/Sprites/buffs/attack");
                HUDSprite = "baf_3";
                break;
            case BuffType.Damage:
                GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Atlas/Sprites/buffs/damage");
                HUDSprite = "baf_2";
                break;
            case BuffType.Protection:
                GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Atlas/Sprites/buffs/protection");
                HUDSprite = "baf";
                break;
            case BuffType.Speed:
                GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Atlas/Sprites/buffs/run");
                HUDSprite = "baf_4";
                break;
            case BuffType.Score:
                GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Atlas/Sprites/buffs/cup");
                HUDSprite = "baf";
                break;
        }
    }

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.CompareTag ("Player")) 
		{
			live = false;
			MasterAudio.PlaySoundAndForget(PicUpSound);
		}
	}

    private IEnumerator Wait()
    {
		var wait = new WaitForSeconds(6f);
		yield return wait;

		if (gameObject) 
		{
			_isBlinking = true;
			var alpha = 0.6f;
			for (var i = 0; i < 14; i++) {
				_rend.color = new Color (_rend.color.r, _rend.color.g, _rend.color.b, alpha);
				alpha = alpha.Equals (1f) ? 0.4f : 1f;
				wait = new WaitForSeconds (0.25f);
				yield return wait;
			}
			_isBlinking = false;
			Destroy (gameObject);
		}
    }

        private void OnStateChange(GameState prev, GameState next)
    {
        if (next != GameState.NotStarted) return;
        StopAllCoroutines();
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        MainGameSingletone.Instance.StateManager.StateChangedEvent -= OnStateChange;
    }

    private Coroutine _wait;

    private void Update()
    {
		if (live)
			MasterAudio.PlaySoundAndForget(FlySound);
		if (transform.localPosition.y > -1.8f)
            transform.position -= transform.up * Time.deltaTime * 2f;
        else if (_wait == null)
            _wait = StartCoroutine(Wait());
    }
}
