﻿using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices;

public class WaveProgressAndHealthPanelScript : MonoBehaviour
{
    [SerializeField] private UISlider _progressBar;
    [SerializeField] private UILabel _progressLabel;
    [SerializeField] private UILabel _healthLabel;

    public void Init()
    {
        _progressBar.value = 0;
        _progressLabel.text = "0";
        _healthLabel.text = "0";

        MainGameSingletone.Instance.WaveManager.NewWaveStarted += OnNewWaveStarted;
        MainGameSingletone.Instance.WaveManager.WaveProgressChanged += OnWaveProgressChanged;
        MainGameSingletone.Instance.PlayerStats.HealthPointsChanged += OnHealthPointsChanged;
    }

    private void OnHealthPointsChanged(int health)
    {
        _healthLabel.text = string.Format("{0}/10", health);
    }

    private void OnWaveProgressChanged(float currentProgress)
    {
        _progressBar.value = currentProgress;
    }

    private void OnNewWaveStarted(int waveNumber)
    {
        _progressBar.value = 0;
        _progressLabel.text = string.Format (Localization.Get("wave_norm"), waveNumber);
    }

    public void Uninit()
    {
        MainGameSingletone.Instance.WaveManager.NewWaveStarted -= OnNewWaveStarted;
        MainGameSingletone.Instance.WaveManager.WaveProgressChanged -= OnWaveProgressChanged;
    }
}
