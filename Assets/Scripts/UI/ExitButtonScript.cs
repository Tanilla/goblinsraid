﻿using UnityEngine;

public class ExitButtonScript : MonoBehaviour
{
	[SerializeField] private GameObject _exitForm;
	[SerializeField] private GameObject _statisticScreen;
	[SerializeField] private UIButton _yesButton;
	[SerializeField] private UIButton _noButton;

	private EventDelegate _yesButtonEventDelegate;
	private EventDelegate _noButtonEventDelegate;

	private void OnEnable()
	{
		if (_yesButtonEventDelegate == null)
			_yesButtonEventDelegate = new EventDelegate(OnYesButtonReleased);
		_yesButton.onClick.Add(_yesButtonEventDelegate);

		if (_noButtonEventDelegate == null)
			_noButtonEventDelegate = new EventDelegate(OnNoButtonReleased);
		_noButton.onClick.Add(_noButtonEventDelegate);
	}

	public void OnButtonReleased()
    {
		if (!_exitForm.gameObject.activeInHierarchy) 
		{
			_exitForm.gameObject.SetActive (true);
			MainGameSingletone.Instance.StateManager.SetGameOnPause ();
		}
    }

	public void OnYesButtonReleased()
	{
		_exitForm.gameObject.SetActive(false);
		MainGameSingletone.Instance.StateManager.SetNotInGame();
		_statisticScreen.gameObject.SetActive(true);
		MainGameSingletone.Instance.UIManager.StatisticScreen.GameResult = 0;
		//MainGameSingletone.Instance.UIManager.GameLobbyWidget.gameObject.SetActive(true);
	}

	public void OnNoButtonReleased()
	{
		_exitForm.gameObject.SetActive(false);
		MainGameSingletone.Instance.StateManager.SetGameInProgress ();
	}
}
