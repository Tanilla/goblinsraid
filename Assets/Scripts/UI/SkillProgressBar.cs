﻿using DarkTonic.MasterAudio;
using UnityEngine;

public class SkillProgressBar : MonoBehaviour
{
    [SerializeField] private UISlider _progressBar;
    [SerializeField] private UIButton _skill;
    [SerializeField] private UIButton _superSkill;

    public void Init()
    {
        _progressBar.value = 0;
        _skill.isEnabled = false;
        _superSkill.isEnabled = false;
        MainGameSingletone.Instance.PlayerStats.SkillPointsChanged += OnSkillPointsChanged;
    }

    private void OnSkillPointsChanged(float skillPoints)
    {
        _progressBar.value = skillPoints >= PlayerStats.SkillCost
            ? skillPoints/PlayerStats.SuperSkillCost + 0.0001f*skillPoints
            : skillPoints/PlayerStats.SuperSkillCost - 0.0009f*skillPoints;
        if (_skill.isEnabled == false && skillPoints >= PlayerStats.SkillCost)
        {
            MasterAudio.PlaySoundAndForget("skill.button.active");
            MainGameSingletone.Instance._playerController._hudText.Add(Localization.Get("skill_hud_text"), Color.yellow,
                0f);
        }
        _skill.isEnabled = skillPoints >= PlayerStats.SkillCost;

        if (_superSkill.isEnabled == false && skillPoints >= PlayerStats.SuperSkillCost)
        {
            MasterAudio.PlaySoundAndForget("superskill.button.active");
            MainGameSingletone.Instance._playerController._hudText.Add(Localization.Get("superskill_hud_text"),
                Color.yellow, 0f);
        }
        _superSkill.isEnabled = skillPoints >= PlayerStats.SuperSkillCost;
    }

    public void Uninit()
    {
        MainGameSingletone.Instance.PlayerStats.SkillPointsChanged -= OnSkillPointsChanged;
    }
}
