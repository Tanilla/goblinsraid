﻿using UnityEngine;

public class EnemyProjectileScript : MonoBehaviour
{

    private void Start() 
	{
        MainGameSingletone.Instance.StateManager.StateChangedEvent += OnStateChange;
	}

    private void OnDestroy()
    {
        MainGameSingletone.Instance.StateManager.StateChangedEvent -= OnStateChange;
    }

	private void Update()
	{
		if (transform.localPosition.y > -1.8f)
			transform.position -= transform.up * Time.deltaTime * MainGameSingletone.Instance.PlayerProjectileMovementSpeed * 0.38f;
		else
			Destroy(gameObject);
	}

    private void OnStateChange(GameState prev, GameState next)
    {
        if (next != GameState.NotStarted) return;
        StopAllCoroutines();
        Destroy(gameObject);
    }
}