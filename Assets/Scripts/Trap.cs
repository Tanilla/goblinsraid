﻿using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;
using System.Collections.Generic;

public class Trap : MonoBehaviour
{
    private readonly HashSet<EnemyClass> _goblins = new HashSet<EnemyClass>();
	private Coroutine _currentWait;
	private Coroutine _dot;
	private float totalLive;
	private float activeLive;
	private float slowly;
	private float dmg;
	private bool _isBlinking;
	private SpriteRenderer _rend;

	private string ActiveSound;

	private IEnumerator Wait(float time) 
	{
		var wait = new WaitForSeconds(time);
		yield return wait;
		Destroy(gameObject);
	}

	private void Start() 
	{
		_rend = GetComponent<SpriteRenderer>();
		totalLive = 90f;
		activeLive = 30f;
		slowly = 3.75f;
		dmg = 5f;
		MainGameSingletone.Instance.StateManager.StateChangedEvent += OnStateChange;
		_currentWait = StartCoroutine(Wait(totalLive));
		ActiveSound = "trap.operate";
	}

    private void OnDestroy()
    {
        foreach (var goblin in _goblins)
        {
            goblin.SpeedModifer = 1f;
        }
		MainGameSingletone.Instance.StateManager.StateChangedEvent -= OnStateChange;
    }

    private void OnStateChange(GameState prev, GameState next)
    {
        if (next != GameState.NotStarted) return;
        StopAllCoroutines();
        Destroy(gameObject);
    }

	private void OnTriggerEnter2D(Collider2D collision) 
	{
		if (!collision.CompareTag("Enemy")) return;
		if (_currentWait == null) return;
		StopCoroutine(_currentWait);
		StartCoroutine(Wait(activeLive));
		StartCoroutine (Blink ());
        GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Atlas/Sprites/Character/Archer/trap4");
	    _goblins.Add(collision.GetComponentInParent<EnemyClass>());
		_dot = StartCoroutine(collision.GetComponentInParent<EnemyClass>().DamageOverTime(dmg));
		collision.GetComponentInParent<EnemyClass>().SpeedModifer = 1f / slowly;
		MasterAudio.PlaySoundAndForget(ActiveSound);
	}

	private IEnumerator Blink()
	{
		yield return new WaitForSeconds (activeLive - 3.5f);
		_isBlinking = true;
		var alpha = 0.6f;
		for (var i = 0; i < 14; i++)
		{
			_rend.color = new Color(_rend.color.r, _rend.color.g, _rend.color.b, alpha);
			alpha = alpha.Equals(1f) ? 0.6f : 1f;
			yield return new WaitForSeconds(0.25f);
		}
		_isBlinking = false;
	}

	private void OnTriggerExit2D(Collider2D collision) 
	{
        if (!collision.CompareTag("Enemy")) return;
        StopCoroutine(_dot);
	    _goblins.Remove(collision.GetComponentInParent<EnemyClass>());
		collision.GetComponentInParent<EnemyClass>().SpeedModifer = 1f;
	}
}
