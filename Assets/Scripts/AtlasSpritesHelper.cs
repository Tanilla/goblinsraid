﻿using UnityEngine;
using System.Collections.Generic;

public class AtlasSpritesHelper : MonoBehaviour
{
    public List<Sprite> BloodSprites = new List<Sprite>(); 

    public Sprite ArcherProjectileSprite;
    public Sprite GoblinProjectileSprite;
}
