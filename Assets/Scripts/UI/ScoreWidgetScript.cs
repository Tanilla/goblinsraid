﻿using UnityEngine;

public class ScoreWidgetScript : MonoBehaviour
{
	[SerializeField] public UILabel _scoreLabel = null;

	public void Init ()
    {
        MainGameSingletone.Instance.PlayerStats.ScoreChanged += OnScoreChanged;
        if(_scoreLabel != null)
	        _scoreLabel.text = "0";
    }

    private void OnScoreChanged(int score)
    {
        if (_scoreLabel != null)
            _scoreLabel.text = score.ToString();
    }

    public void Uninit()
    {
        MainGameSingletone.Instance.PlayerStats.ScoreChanged -= OnScoreChanged;
    }
}
