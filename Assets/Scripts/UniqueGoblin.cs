using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;

public class UniqueGoblin : EnemyClass
{
    private bool _isTeleporting;
	private float _attackDelay;

    protected override void OnEnable()
    {
        base.OnEnable();
        ProjectilePrefabPath = "Prefabs/UniqueProjectilePrefab";
        RuntimeControllerPath = "Animations/UniqueAnimations/UniqueAnimController";
        ShootSound = "None";
        Animator.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>(RuntimeControllerPath);
        DropableBuffs = new[] {Buff.BuffType.Ammunition, Buff.BuffType.Attack, Buff.BuffType.Damage, Buff.BuffType.Protection, Buff.BuffType.Speed};
		HP = 12;
		BaseSpeed = 0.32f;
		DropChance = 1f;
	    PointsForKill = 30;
	    PenaltyPoints = 30;
		PointsForHit = 1;
        Speed = BaseSpeed;
		StepSound = "step.goblin";
    }

	protected IEnumerator OnBecameVisible ()
	{
		while (true)
		{
			if (Timer) {
				_attackDelay = 5 + Mathf.Round (Random.Range (0, 8));
				Timer = false;
				yield return new WaitForSeconds (_attackDelay);
				Timer = true;

				if (transform.localPosition.y > 0.3f) {
					Animator.SetTrigger ("Attack");
					attackNow = true;
				}
			} else
				break;
		}
	}

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
        if (gameObject.activeInHierarchy) StartCoroutine(Teleport());
    }

    private IEnumerator Teleport()
    {
        if (_isTeleporting) yield break;
        _isTeleporting = true;
        var wait = new WaitForSeconds(Random.Range(3f, 3f));
        yield return wait;
        Animator.SetTrigger("Teleport");
        _isTeleporting = false;
    }

    public override void Death()
    {
        base.Death();
        MainGameSingletone.Instance.WaveManager.IsUniqueExists = false;
    }
}
