using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using DarkTonic.MasterAudio;
using UnityEngine;
using Random = UnityEngine.Random;

public class WaveManager : MonoBehaviour, IManager
{
    public event Action<int> NewWaveStarted;
    public event Action<float> WaveProgressChanged;

    private int _currentWave = 0;
    public int CurrentWave
    {
        get { return _currentWave; }
    }
	[SerializeField] public int _waveNumber;
    private int _phaseNumber;
    private int _probabilitiesShift;
	private float _goblinsInWave;
    private float _goblinsInPhase;
    private float _spawnDelay;
    private float _spawnChance;
    private float _phaseStartTime;
    private float _waveStartTime;
    private float[,] _probabilities;
    private bool _isWaveInProgress;
	private int ScoreForHPBonus;
	public bool showVictoryScreen;
	private float _prevHP;
	private float _curHP;
	private int _rewardScore;
	private int _rewardStat;
    [HideInInspector] public int DeadEnemies;
    [HideInInspector] public bool IsUniqueExists;
	[HideInInspector] public bool IsRareExists;
	private int r;

    private readonly List<EnemyClass> _listOfEnemies = new List<EnemyClass>();
    private const string GoblinPrefabPath = "Prefabs/GoblinPrefab";

    private void ClearLastWave()
    {
        _listOfEnemies.Clear();
    }

    public void Reset()
    {
        MasterAudio.FireCustomEvent("AmbientFadeOut", transform.position);
        StopAllCoroutines();
        _listOfEnemies.ForEach(z => z.EnemyDiedEvent -= CheckIfLastEnemyDied);
        _listOfEnemies.ForEach(z => z.Despawn());
        _probabilitiesShift = 0;
        _currentWave = 0;
        _waveNumber = 0;
        _phaseNumber = 0;
        _goblinsInWave = 0;
        _goblinsInPhase = 0;
        _phaseStartTime = 0;
        _waveStartTime = 0;
        _isWaveInProgress = false;
        DeadEnemies = 0;
        IsUniqueExists = false;
        IsRareExists = false;
		_curHP = MainGameSingletone.Instance.PlayerStats.PlayerHealthPoints;
		_prevHP = _curHP;
        ClearLastWave();
		showVictoryScreen = false;
    }

    public void StartWaving()
    {
		_curHP = MainGameSingletone.Instance.PlayerStats.PlayerHealthPoints;
		_prevHP = _curHP;
		StartCoroutine (StartNewWave ());
    }

    private IEnumerator StartNewWave()
    {
        MasterAudio.FireCustomEvent("AmbientFadeIn", transform.position);
		_curHP = MainGameSingletone.Instance.PlayerStats.PlayerHealthPoints;

		if (_waveNumber == 0) {
			if (NewWaveStarted != null)
				NewWaveStarted(_waveNumber + 1);
		}
		
        _isWaveInProgress = false;
        _waveStartTime = Time.time;
        _waveNumber++;

		yield return StartCoroutine (WaveProgress ());

        if (_waveNumber == 5 || _waveNumber == 6 || _waveNumber == 7 || _waveNumber == 9)
            _probabilitiesShift++;
		
        _phaseNumber = 0;
        DeadEnemies = 0;

		switch (_waveNumber)
		{
		default:
			_goblinsInWave = _waveNumber + 30;
			break;
		case 1:
			_goblinsInWave = 8;
			break;
		case 2:
			_goblinsInWave = 10;
			break;
		case 3:
			_goblinsInWave = 13;
			break;
		case 4:
			_goblinsInWave = 16;
			break;
		case 5:
			_goblinsInWave = 22;
			break;
		case 6:
			_goblinsInWave = 25;
			break;
		case 7:
			_goblinsInWave = 28;
			break;
		case 8:
			_goblinsInWave = 31;
			break;
		case 9:
			_goblinsInWave = 33;
			break;
		case 10:
			_goblinsInWave = 39;
			break;
			//Mathf.Round (31 * Mathf.Sqrt (_waveNumber / 5.5f));
		}

        _goblinsInPhase = _goblinsInWave / 3;
        MasterAudio.PlaySoundAndForget("roar");
        StartCoroutine(MainGameSingletone.Instance.UIManager.MessageWidget.ShowMessage("wave_caps", 2f, _waveNumber));
        _isWaveInProgress = true;
        StartCoroutine(SpawnUnique());
        yield return StartCoroutine(StartPhase());
    }

	public IEnumerator WaveProgress()
    {
        _curHP = MainGameSingletone.Instance.PlayerStats.PlayerHealthPoints;

		if (_waveNumber > 1) 
		{
			MainGameSingletone.Instance.PlayerStats.TotalWaveNumber = _waveNumber;

			MasterAudio.PlaySoundAndForget ("wave.complete");

			if (_curHP < _prevHP) 
			yield return StartCoroutine (
					MainGameSingletone.Instance.UIManager.MessageWidget.ShowMessage ("not_bad", 2f));
			else
				yield return StartCoroutine (
					MainGameSingletone.Instance.UIManager.MessageWidget.ShowMessage ("excelent", 2f));

			_prevHP = _curHP;

			if (MainGameSingletone.Instance.StateManager.CurrentState == GameState.Pause)
				yield return new WaitForSeconds (0.25f);
			
			if (_waveNumber == 21) 
			{
				MainGameSingletone.Instance.StateManager.SetGameOnPause ();
				MainGameSingletone.Instance.UIManager.StatisticScreen.GameResult = 1;
				MainGameSingletone.Instance.UIManager.VictoryForm.gameObject.SetActive (true);
			}

			if (MainGameSingletone.Instance.StateManager.CurrentState == GameState.Pause)
				yield return new WaitForSeconds (0.25f);

			MainGameSingletone.Instance.UIManager.ChooseReward.gameObject.SetActive (true);
			MainGameSingletone.Instance.StateManager.SetGameOnPause ();

			if (MainGameSingletone.Instance.StateManager.CurrentState == GameState.Pause)
				yield return new WaitForSeconds (0.25f);

			yield return StartCoroutine (MainGameSingletone.Instance.UIManager.MessageWidget.Countdown ());

			if (NewWaveStarted != null)
				NewWaveStarted (_waveNumber);
		}
    }

    private void CheckIfLastEnemyDied(EnemyClass enemy)
    {
        _listOfEnemies.Remove(enemy);

        if (WaveProgressChanged != null)
            WaveProgressChanged(DeadEnemies / _goblinsInWave);
		
		if (_goblinsInWave.Equals (DeadEnemies)) 
		{
			StartCoroutine(StartNewWave());
		}
		else if (_listOfEnemies.Count <= 1)
        {
			_phaseNumber++;
            StartCoroutine(StartPhase());
        }
        else if (_listOfEnemies.Count <= 2 && _phaseStartTime + _spawnDelay < Time.time)
        {
            _phaseNumber++;
			StartCoroutine(StartPhase());
        }
    }

    private IEnumerator StartPhase()
    {
        _phaseStartTime = Time.time;
        float inPhase;

        switch (_phaseNumber)
        {
            case 0:
				inPhase = Mathf.Ceil(_goblinsInPhase);
                break;
            case 1:
                inPhase = Mathf.Round(_goblinsInPhase);
                break;
            case 2:
				inPhase = Mathf.Floor(_goblinsInPhase);
                break;
            default:
                inPhase = 0;
                break;
        }

        while (inPhase > 0)
        {
			var toSpawn = Mathf.Clamp (Mathf.Round (Mathf.Sqrt (_waveNumber)) + MainGameSingletone.Instance.BuffSystem.BuffCount, 0, inPhase);
            
			for (var i = 0; i < toSpawn; i++)
            {
                var go = MainGameSingletone.Instance.EnemyPool.PoolObject;
                if (go == null) continue;
                var chance = Random.value;
                if (chance < _probabilities [_phaseNumber + _probabilitiesShift, 2] && !IsRareExists && _waveNumber > 8)
                {
					go.AddComponent<RareGoblin> ();
					IsRareExists = true;
				}
                else if (chance < _probabilities [_phaseNumber + _probabilitiesShift, 1] && _waveNumber > 1)
                {
					go.AddComponent<UncommonGoblin> ();
				}
                else
                {
					go.AddComponent<CommonGoblin> ();
				}
				var enemyClassObject = go.GetComponent<EnemyClass>();
				enemyClassObject.EnemyDiedEvent += CheckIfLastEnemyDied;
				_listOfEnemies.Add(enemyClassObject);
			}

            inPhase -= toSpawn;
            _listOfEnemies.ForEach(x => { if (x != null) x.gameObject.SetActive(true); });
            _spawnDelay = _waveNumber / 5.0f;
            yield return new WaitForSeconds(_spawnDelay);
        }
    }

    private IEnumerator SpawnUnique()
    {
		while (_isWaveInProgress && _waveNumber > 4 && (DeadEnemies + 5) < _goblinsInWave)
        {
			if (!IsUniqueExists && _waveNumber > 1)
            {
                _spawnChance = MainGameSingletone.Instance.BuffSystem.BuffCount < 3
                    ? 0.12f + (3 * Mathf.Sqrt(_waveNumber))
                    : 0.05f;
				if (Random.value <= _spawnChance && _waveStartTime + 1.25f < Time.time)
                {
                    var go = MainGameSingletone.Instance.EnemyPool.PoolObject;
                    if (go != null)
                    {
                        go.AddComponent<UniqueGoblin>();
                        var enemyClassObject = go.GetComponent<EnemyClass>();
                        enemyClassObject.EnemyDiedEvent += CheckIfLastEnemyDied;
                        _listOfEnemies.Add(enemyClassObject);
                        _goblinsInWave++;
                        IsUniqueExists = true;
                        go.SetActive(true);
                    }
                }
            }
            yield return new WaitForSeconds(1f);
        }
    }

    public void Init()
    {
        _probabilities = new[,] { { 1.00f, 0.00f, 0.00f },
                                  { 0.75f, 0.25f, 0.00f },
                                  { 0.73f, 0.27f, 0.00f },
                                  { 0.31f, 0.29f, 1.00f },
                                  { 0.24f, 0.31f, 1.00f },
                                  { 0.18f, 0.33f, 1.00f },
                                  { 0.12f, 0.35f, 1.00f } };

        MainGameSingletone.Instance.StateManager.StateChangedEvent += OnStateChanged;
    }

    private void OnStateChanged(GameState prevState, GameState newState)
    {
        if (prevState == GameState.NotStarted && newState == GameState.InProgress)
            StartWaving();
        if (newState == GameState.NotStarted)
            Reset();
    }

	public void Uninit()
    {
        StopAllCoroutines();
        _listOfEnemies.ForEach(z => z.EnemyDiedEvent -= CheckIfLastEnemyDied);
        MainGameSingletone.Instance.StateManager.StateChangedEvent -= OnStateChanged;
    }
}