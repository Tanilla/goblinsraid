using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;

public class UncommonGoblin : EnemyClass
{
	private float _attackDelay;
	private bool _stage;
	private float _speedStage1;
	private float _hpTreshold;

    protected override void OnEnable()
    {
        base.OnEnable();
        ProjectilePrefabPath = "Prefabs/UncommonProjectilePrefab";
	    RuntimeControllerPath = "Animations/UncommonAnimations/UncommonAnimController";
        ShootSound = "uncommon.attack";
        Animator.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>(RuntimeControllerPath);
		HP = 8;
		BaseSpeed = 0.36f;
	    PointsForKill = 15;
	    PenaltyPoints = 15;
		PointsForHit = 2;
	    InvokeDelay = 0.7f;
		Speed = BaseSpeed;
	    _hpTreshold = HP / 2;
		_speedStage1 = BaseSpeed * 3.25f;//2.1f;
		ShootSound = "uncommon.attack";
		StepSound = "step.goblin";
    }

	protected IEnumerator OnBecameVisible ()
	{
		while (true)
		{
			if (Timer) 
			{
				_attackDelay = 3 + Mathf.Round (Random.Range (0, 7));
				Timer = false;
				yield return new WaitForSeconds (_attackDelay);
				Timer = true;

				if (transform.localPosition.y > 0.3f && HP > _hpTreshold) 
				{
					Animator.SetTrigger ("Attack");
					HP = _hpTreshold;
					Speed = _speedStage1;
				    _stage = true;
					attackNow = true;
					MasterAudio.PlaySoundAndForget("uncommon.bloodlust");
				}
			} 
			else
				break;
		}
	}

    protected override void OnTriggerEnter2D(Collider2D collision)
	{
		base.OnTriggerEnter2D(collision);
		if (HP <= _hpTreshold && !_stage && !IsDead)
			ChangeStage();
	}

	private void ChangeStage()
	{
	    MasterAudio.PlaySoundAndForget("uncommon.bloodlust");
	    Animator.SetTrigger("StageB");
	    Speed = _speedStage1;
	    _stage = true;
	}
}
