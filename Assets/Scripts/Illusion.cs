﻿using UnityEngine;
using System.Collections;

public class Illusion : MonoBehaviour
{
	public static int IllusionsCount { get; private set; }
	private static float _shift = 0.8f;
	private GameObject _player;

	private void Start ()
	{
		_player = GameObject.FindWithTag("Player");
		transform.parent = _player.transform;
		IllusionsCount++;
		transform.position = new Vector3(_player.transform.position.x + _shift, _player.transform.position.y - 0.3f);
		_shift *= -1;
		StartCoroutine(Fire());
	}
	
	private void OnDestroy ()
	{
		IllusionsCount--;
	}

	private IEnumerator Fire()
	{
		while (gameObject)
		{
			yield return new WaitForSeconds(MainGameSingletone.Instance.PlayerAttackDelay * 1.5f);
			var projectile = Resources.Load("Prefabs/PlayerProjectilePrefab");
			var go = Instantiate(projectile) as GameObject;
			var arrow = (PlayerProjectileScript)FindObjectOfType(typeof(PlayerProjectileScript));
			arrow.Dmg = 1.0f * MainGameSingletone.Instance.ArrowDamage;
			arrow.transform.position = new Vector3(transform.position.x + 0.2f, transform.position.y);
			arrow.transform.localScale = Vector3.one;
		}
	}
}
