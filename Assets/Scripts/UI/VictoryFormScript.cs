﻿using UnityEngine;
using System.Collections;

public class VictoryFormScript : MonoBehaviour 
{
	[SerializeField] private UIButton _continueButton;
	[SerializeField] private UIButton _quitButton;
	[SerializeField] private GameObject _statisticsScreen;
	[SerializeField] private UILabel _finalScoreLabel = null;

	private EventDelegate _continueButtonEventDelegate;
	private EventDelegate _quitButtonEventDelegate;

	private void Start()
	{
		if (_finalScoreLabel != null)
			_finalScoreLabel.text = MainGameSingletone.Instance.UIManager.ScoreWidget._scoreLabel.text;
	}

	private void OnEnable()
	{
		if (_continueButtonEventDelegate == null)
			_continueButtonEventDelegate = new EventDelegate(OnContinueButtonReleased);
		_continueButton.onClick.Add(_continueButtonEventDelegate);

		if (_quitButtonEventDelegate == null)
			_quitButtonEventDelegate = new EventDelegate(OnQuitButtonReleased);
		_quitButton.onClick.Add(_quitButtonEventDelegate);

		MainGameSingletone.Instance.WaveManager.showVictoryScreen = true;
	}

	private void OnDisable()
	{
		_continueButton.onClick.Remove(_continueButtonEventDelegate);
		_quitButton.onClick.Remove(_quitButtonEventDelegate);
		MainGameSingletone.Instance.WaveManager.showVictoryScreen = false;
	}

	private void OnContinueButtonReleased()
	{
		MainGameSingletone.Instance.StateManager.SetGameInProgress ();
		gameObject.SetActive(false);
	}

	private void OnQuitButtonReleased()
	{
		MainGameSingletone.Instance.StateManager.SetNotInGame();
		_statisticsScreen.gameObject.SetActive (true);
		gameObject.SetActive(false);
	}
}
