using UnityEngine;

public class MainGameSingletone : MonoBehaviour
{
    public float PlayerMovementSpeed;
    public float EnemyMovementSpeed;
    public float PlayerProjectileMovementSpeed;
	public float PlayerAttackDelay;
	public float ArrowDamage;
	public int PointsForDeath;
    [HideInInspector] public BuffSystem BuffSystem;
    [SerializeField] private BuffHUD _buffHud;


    public WaveManager WaveManager;
    public UIManager UIManager;
    public EnemyPoolManager EnemyPool;
    [SerializeField] public PlayerController _playerController;
    [HideInInspector] public PlayerStats PlayerStats;
    [HideInInspector] public readonly GameStatesManager StateManager = new GameStatesManager();

	#region Singletone
	private static MainGameSingletone _instance;
    public static MainGameSingletone Instance
    {
        get { return _instance ?? (_instance = new MainGameSingletone()); }
    }
    #endregion

    public void Awake()
    {
        _instance = this;
        PlayerStats = new PlayerStats();
        BuffSystem = GetComponent<BuffSystem>();
    }

	public void Init()
    {
        StateManager.Init();
        UIManager.Init();
        PlayerStats.Init();
        WaveManager.Init();
        BuffSystem.Init();
        EnemyPool.Init();
        _playerController.Init();
        _buffHud.Init();
		Resources.LoadAll("Atlas");
		Resources.LoadAll("Animations");
		Resources.LoadAll("Prefabs");
    }

    public void Uninit()
    {
        StateManager.Uninit();
        UIManager.Uninit();
        PlayerStats.Uninit();
        WaveManager.Uninit();
        BuffSystem.Uninit();
        EnemyPool.Uninit();
        _playerController.Uninit();
		Resources.UnloadUnusedAssets ();
    }
}
