﻿using UnityEngine;
using System.Collections;

public class StatisticScreenScript : MonoBehaviour
{
	[SerializeField] private UIButton _replayButton;
	[SerializeField] private UIButton _quitButton;
	[SerializeField] private UILabel _finalScoreLabel = null;
	[SerializeField] private GameObject _leaveFlag;
	[SerializeField] private GameObject _victoryFlag;
	[SerializeField] private GameObject _defeatFlag;

	[SerializeField] public UILabel _kills;
	[SerializeField] public UILabel _maxCombo;
	[SerializeField] public UILabel _maxWave;
	[SerializeField] public UILabel _bestScore;
	[SerializeField] public UILabel _deaths;
	[SerializeField] public UILabel _accuracy;
	[SerializeField] public UILabel _skills;
	[SerializeField] public UILabel _superSkills;

	private string _accuracyText;

	private EventDelegate _replayButtonEventDelegate;
	private EventDelegate _quitButtonEventDelegate;

	private int finalScore;
	public int GameResult;

	private void Start()
	{
		MainGameSingletone.Instance.UIManager.BattleUI.gameObject.SetActive (false);
		MainGameSingletone.Instance.UIManager.GameLobbyWidget.gameObject.SetActive (false);
	}

	private void OnEnable()
	{
		_accuracyText = string.Format("{0:0.00}",MainGameSingletone.Instance.PlayerStats.TotalAccuracy * 100);
		MainGameSingletone.Instance.UIManager.BattleUI.gameObject.SetActive (false);

		/*
		 * GameResult
		 * 0 = leave
		 * 1 = victory
		 * 2 = defeat
		*/
		switch (GameResult)
		{
		default:
			_leaveFlag.gameObject.SetActive (true);
			break;
		case 1:
			_victoryFlag.gameObject.SetActive (true);
			MainGameSingletone.Instance.PlayerStats.Score *= 2;
			break;
		case 2:
			_defeatFlag.gameObject.SetActive (true);
			break;
		}

		if (_kills != null)
			_kills.text = MainGameSingletone.Instance.PlayerStats.TotalKills.ToString ();
		if (_maxCombo != null)
			_maxCombo.text = MainGameSingletone.Instance.PlayerStats.TotalMaxCombo.ToString ();
		if (_maxWave != null)
			_maxWave.text = MainGameSingletone.Instance.PlayerStats.TotalWaveNumber.ToString ();
		if (_bestScore != null)
			_bestScore.text = MainGameSingletone.Instance.PlayerStats._bestScore.ToString ();
		if (_deaths != null)
			_deaths.text = MainGameSingletone.Instance.PlayerStats.TotalDeaths.ToString ();
		if (_accuracy != null)
			_accuracy.text = _accuracyText.ToString () + "%";
		if (_skills != null)
			_skills.text = MainGameSingletone.Instance.PlayerStats.TotalSkillUse.ToString ();
		if (_superSkills != null)
			_superSkills.text = MainGameSingletone.Instance.PlayerStats.TotalSuperSkillUse.ToString ();


		if (_replayButtonEventDelegate == null)
			_replayButtonEventDelegate = new EventDelegate(OnReplayButtonReleased);
		_replayButton.onClick.Add(_replayButtonEventDelegate);

		if (_quitButtonEventDelegate == null)
			_quitButtonEventDelegate = new EventDelegate(OnQuitButtonReleased);
		_quitButton.onClick.Add(_quitButtonEventDelegate);

		finalScore = MainGameSingletone.Instance.PlayerStats.Score;
		if (_finalScoreLabel != null)
			_finalScoreLabel.text = finalScore.ToString();
	}

	private void OnDisable()
	{
		_replayButton.onClick.Remove(_replayButtonEventDelegate);
		_quitButton.onClick.Remove(_quitButtonEventDelegate);
		_leaveFlag.gameObject.SetActive (false);
		_victoryFlag.gameObject.SetActive (false);
		_defeatFlag.gameObject.SetActive (false);
	}

	private void OnReplayButtonReleased()
	{
		MainGameSingletone.Instance.StateManager.SetGameInProgress ();
		MainGameSingletone.Instance.UIManager.BattleUI.gameObject.SetActive (true);
		//MainGameSingletone.Instance.UIManager.GameLobbyWidget.gameObject.SetActive (true);
		//MainGameSingletone.Instance.UIManager.GameLobbyWidget._mainMenu.gameObject.SetActive (false);
		//MainGameSingletone.Instance.UIManager.GameLobbyWidget._charChoose.gameObject.SetActive (true);
		gameObject.SetActive(false);
	}

	private void OnQuitButtonReleased()
	{
		MainGameSingletone.Instance.UIManager.GameLobbyWidget.gameObject.SetActive (true);
		MainGameSingletone.Instance.UIManager.BattleUI.gameObject.SetActive (false);
		gameObject.SetActive(false);
	}
}

