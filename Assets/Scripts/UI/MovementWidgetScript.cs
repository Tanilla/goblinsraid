﻿using UnityEngine;

public class MovementWidgetScript : MonoBehaviour
{
    private EventDelegate _stickMoveEventDelegate;
    private EventDelegate _stickDropEventDelegate;
    private Vector3 _startPos;
    private const float _deadZone = 80f;
	public float SpeedModifer;

    [HideInInspector] public float Axis;

    private void OnEnable()
    {
        _startPos = transform.localPosition;
        if (_stickMoveEventDelegate == null)
            _stickMoveEventDelegate = new EventDelegate(OnMove);
        GetComponent<UIEventTrigger>().onDrag.Add(_stickMoveEventDelegate);

        if (_stickDropEventDelegate == null)
            _stickDropEventDelegate = new EventDelegate(OnMoveEnd);
        GetComponent<UIEventTrigger>().onDragEnd.Add(_stickDropEventDelegate); 

		SpeedModifer = 1f;
    }

    private void OnDisable()
    {
        GetComponent<UIEventTrigger>().onDragEnd.Remove(_stickDropEventDelegate);
        GetComponent<UIEventTrigger>().onDrag.Remove(_stickMoveEventDelegate);
        Axis = 0f;
		SpeedModifer = 1f;
    }

    private void OnMove()
    {
        if (transform.localPosition.x < _startPos.x + _deadZone && transform.localPosition.x > _startPos.x - _deadZone)
        {
            Axis = 0f;
			SpeedModifer = 1f;
            return;
        }
        Axis = transform.localPosition.x > _startPos.x ? 1f : -1f;
		SpeedModifer = Mathf.Abs (transform.localPosition.x) / 346f;
    }

    private void OnMoveEnd()
    {
        transform.localPosition = _startPos;
        Axis = 0f;
		SpeedModifer = 1f;
    }
}
