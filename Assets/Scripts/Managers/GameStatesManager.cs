﻿using System;
using UnityEngine;

public enum GameState
{
    NotStarted,
    InProgress,
    Pause
}

public class GameStatesManager : IManager
{
    private GameState _currentState;

    public GameState CurrentState
    {
        get {return _currentState;}
        private set
        {
            var prevState = _currentState;
            _currentState = value;
            if (StateChangedEvent != null)
                StateChangedEvent(prevState, _currentState);
        }
    }
    public event Action<GameState, GameState> StateChangedEvent;

    public void SetGameOnPause()
    {
        Time.timeScale = 0f;
        CurrentState = GameState.Pause;
    }

    public void SetGameInProgress()
    {
        Time.timeScale = 1f;
        CurrentState = GameState.InProgress;
    }

    public void SetNotInGame()
    {
        Time.timeScale = 1f;
        CurrentState = GameState.NotStarted;
    }

    public void Init()
    {
        CurrentState = GameState.NotStarted;
    }

    public void Uninit()
    { }
}