using System.Collections;
using DarkTonic.MasterAudio;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyClass : MonoBehaviour
{
    private bool IsBuffed { get; set; }
    public HUDText _hudText;

    protected string ProjectilePrefabPath;
    protected Animator Animator;

    protected float BaseSpeed;
    protected int PointsForKill;
    protected int PenaltyPoints;
    protected float PointsForHit;
    protected float DropChance;
	protected bool Timer;
    protected Buff.BuffType[] DropableBuffs;
    protected string RuntimeControllerPath;
    protected bool IsReflecting;
    protected float HP { get; set; }

    public delegate void EnemyDied(EnemyClass enemy);
    public event EnemyDied EnemyDiedEvent;
    public float Speed { get; set; }
	public float SpeedModifer { get; set; }
    public bool InAura { get; set; }
    public int Penalty { get { return PenaltyPoints; } }
    public bool IsDead { get { return Animator.GetBool("Dead"); } }
    [HideInInspector] public float InvokeDelay;
	public bool live;
	public bool attackNow;
	private float yPos;

	public string StepSound;
	protected string ShootSound;
	protected string HitSound;
    
    protected virtual void OnEnable()
    {
        _hudText = GetComponent<HUDTextManager>().HudText;
        Animator = GetComponent<Animator>();
		yPos = Random.Range (6.6f, 9f);
		transform.localPosition = new Vector3(Random.Range(-5f, 5f),yPos);
		GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt(-yPos);
		Timer = true;
		live = true;
		attackNow = false;
		HitSound = "hit.character";
		SpeedModifer = 1f;
    }

    protected virtual void OnDisable()
    {
        GetComponent<SpriteRenderer>().color = Color.white;
        Destroy(this);
    }

	public IEnumerator DamageOverTime(float dmg)
	{
		while (gameObject.activeInHierarchy)
		{
			Animator.SetTrigger ("Stun");

			if (HP > 0) 
			{
				HP -= dmg;
			}
			else if (!IsDead)
		    {
				MainGameSingletone.Instance.PlayerStats.TotalKills++;
				MainGameSingletone.Instance.PlayerStats.AddPointsToScore (PointsForKill);
                _hudText.Add(string.Format("+{0}", PointsForKill), Color.white, 0f);
                Animator.SetBool("Dead", true);
				Drop ();
                yield break;
            }
            var wait = new WaitForSeconds(1f);
			yield return wait;
		}
	}

    protected virtual void Shoot()
    {
        MasterAudio.PlaySoundAndForget(ShootSound);
		var projectile = Resources.Load(ProjectilePrefabPath);
        var go = Instantiate(projectile) as GameObject;
        if (go == null) return;
        go.transform.parent = transform.parent;
        go.transform.localPosition = new Vector3(transform.localPosition.x - 0.2f, transform.localPosition.y);
        go.transform.localScale = Vector3.one;
		attackNow = false;
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (IsDead) return;
        if (collision.CompareTag("Player"))
        {
			if (!MainGameSingletone.Instance.PlayerStats.IsInvincible || !MainGameSingletone.Instance._playerController._isBlinking) 
			{
				MainGameSingletone.Instance.PlayerStats.AddPointsToScore (-MainGameSingletone.Instance.PointsForDeath);
			}
            Animator.SetBool("Dead", true);
        }
        else if (!collision.CompareTag("Trap"))
        {
			MainGameSingletone.Instance.PlayerStats.CurrentShotsInNumber++;
			MainGameSingletone.Instance.PlayerStats.TotalAccuracy = MainGameSingletone.Instance.PlayerStats.CurrentShotsInNumber / MainGameSingletone.Instance.PlayerStats.CurrentShotsAllNumber;
			var playerProjectileDmg = collision.gameObject.GetComponent<PlayerProjectileScript> ().Dmg;
			float critDmg = Mathf.RoundToInt (playerProjectileDmg * MainGameSingletone.Instance.PlayerStats.PlayerCritDmg);
			if (Random.value < MainGameSingletone.Instance.PlayerStats.PlayerCritChance && HP > 1) 
			{
				HP -= critDmg;
				_hudText.Add("CRIT!", Color.red, 0f);
			}
			else
				HP -= playerProjectileDmg;
			MasterAudio.PlaySoundAndForget (HitSound);

			if (IsReflecting && collision.gameObject.GetComponent<PlayerProjectileScript>().SuperArrow == false) 
			{
				Animator.SetTrigger ("reflect");
				collision.gameObject.transform.Rotate (0f, 0f, 180f);
				collision.gameObject.layer = 9;
				collision.gameObject.GetComponent<PlayerProjectileScript>().SpeedModifer = 0.38f;
			}
            else if (collision.gameObject.GetComponent<PlayerProjectileScript>().SuperArrow == false)
				Destroy (collision.gameObject);
			
			MainGameSingletone.Instance.PlayerStats.AddSkillPoints(PointsForHit *
						MainGameSingletone.Instance.PlayerStats.PlayerIntelligence);
            Animator.SetTrigger("Stun");
            if (!(HP <= 0)) return;
            MainGameSingletone.Instance.PlayerStats.AddPointsToScore(PointsForKill);
            _hudText.Add(string.Format("+{0}", PointsForKill), Color.white, 0f);
            Drop();
			MainGameSingletone.Instance.PlayerStats.TotalKills++;
            Animator.SetBool("Dead", true);
        }
    }

    public virtual void Death()
    {
        StopAllCoroutines();
		live = false;
		if (EnemyDiedEvent != null) 
			EnemyDiedEvent (this);
    }

    private void Drop()
    {
        if (!(Random.value <= DropChance) || DropableBuffs == null) return;
        var res = Resources.Load<GameObject>("Prefabs/BuffPrefab");
        var buff = Instantiate(res, transform.position, transform.rotation) as GameObject;
        if (buff != null)
            buff.GetComponent<Buff>().Type = DropableBuffs[Random.Range(0, DropableBuffs.Length)];
    }

    public void Despawn()
    {
        StopAllCoroutines();
        gameObject.SetActive(false);
        Destroy(GetComponent<EnemyClass>());
    }
}