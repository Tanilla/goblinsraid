using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;

public class PlayerController : MonoBehaviour, IManager
{
    protected const string ProjectilePrefabPath = "Prefabs/PlayerProjectilePrefab";
    protected bool _isArrowExists;
    protected bool _canAttack = true;
    protected bool _isCasting;
    public bool _isBlinking;
    protected float _timer;
    protected SpriteRenderer _rend;
    protected Animator _animator;
    public HUDText _hudText;

    protected readonly int[] _scorePoints = new[] {100, 150, 200, 250, 300, 350, 400, 500, 600};
    protected EventDelegate _skillEventDelegate;
    protected EventDelegate _superSkillEventDelegate;
    protected float _totalSpeed;
    public float delaySE;

    [SerializeField] public GameObject _bubble;
    [SerializeField] public GameObject _superEffect;
    [SerializeField] private UIButton _skill;
    [SerializeField] private UIButton _superSkill;

    protected string BowShootSound;
    protected string DeadSound;
    protected string StepSound;
    protected string SkillSound;
    protected string SuperSkillSound;

    protected virtual void Start()
    {
        _animator = GetComponent<Animator>();
        _rend = GetComponent<SpriteRenderer>();
        _totalSpeed = Time.deltaTime*MainGameSingletone.Instance.PlayerStats.PlayerSpeed;
        _isBlinking = MainGameSingletone.Instance.PlayerStats.IsBlinking;
        _hudText = GetComponent<HUDTextManager>().HudText;
    }

    private void Update()
    {
        _totalSpeed = Time.deltaTime*MainGameSingletone.Instance.PlayerStats.PlayerSpeed*
                      MainGameSingletone.Instance.UIManager.MovementWidget.SpeedModifer;

        if (MainGameSingletone.Instance.StateManager.CurrentState != GameState.InProgress) return;
        if (_isCasting) return;
        if ((Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow) ||
             MainGameSingletone.Instance.UIManager.MovementWidget.Axis < 0) &&
            gameObject.transform.localPosition.x > -5f) {
            _animator.SetBool("stay", false);
            _animator.SetBool("walkRight", false);
            _animator.SetBool("walkLeft", true);
            _animator.speed = 0.5f/MainGameSingletone.Instance.PlayerStats.PlayerAttackDelay;
            gameObject.transform.localPosition -= transform.right*_totalSpeed;
            MasterAudio.PlaySound(StepSound);
        }
        else if ((Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow) ||
                  MainGameSingletone.Instance.UIManager.MovementWidget.Axis > 0) &&
                 gameObject.transform.localPosition.x < 5f) {
            _animator.SetBool("stay", false);
            _animator.SetBool("walkLeft", false);
            _animator.SetBool("walkRight", true);
            _animator.speed = 0.5f/MainGameSingletone.Instance.PlayerStats.PlayerAttackDelay;
            gameObject.transform.localPosition += transform.right*_totalSpeed;
            MasterAudio.PlaySound(StepSound);
        }
        else {
            _animator.SetBool("stay", true);
            _animator.SetBool("walkLeft", false);
            _animator.SetBool("walkRight", false);
        }

        if (_isArrowExists) {
            _canAttack = Time.time > _timer + MainGameSingletone.Instance.PlayerStats.PlayerAttackDelay;
            _isArrowExists = !_canAttack;
        }
        else if ((Input.GetKey(KeyCode.Space) || AttackButtonScript.ButtonPress) && _canAttack && !_isCasting) {
            StartCoroutine(Fire());
            _isArrowExists = true;
            _timer = Time.time;
            _animator.SetBool("Attack", true);
            _animator.speed = 1f/MainGameSingletone.Instance.PlayerStats.PlayerAttackDelay;
            MasterAudio.PlaySound(BowShootSound);
        }
        else if ((!Input.GetKey(KeyCode.Space) || !AttackButtonScript.ButtonPress)) {
            _totalSpeed = Time.deltaTime*MainGameSingletone.Instance.PlayerStats.PlayerSpeed;
            _animator.SetBool("Attack", false);
            _animator.speed = 1f;
        }
        if (Input.GetKeyDown(KeyCode.Q))
            Skill();
        if (Input.GetKeyDown(KeyCode.W))
            SuperSkill();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.CompareTag("Buff")) {
            var buff = collision.gameObject.GetComponent<Buff>();

            if (buff.Type != Buff.BuffType.Score) {
                MainGameSingletone.Instance.BuffSystem.AddBuff(buff);
            }
            else {
                //просто указывает число, а не реальную прибавку к скору
                var endScore = _scorePoints[Random.Range(0, _scorePoints.Length)];
                MainGameSingletone.Instance.PlayerStats.AddPointsToScore(endScore);
                _hudText.Add(string.Format("+{0}", endScore), Color.white, 0f);
            }
            Destroy(collision.gameObject);
            return;
        }
        if (MainGameSingletone.Instance.PlayerStats.IsInvincible || _isBlinking) return;
        if (collision.GetComponent<EnemyClass>() != null && collision.GetComponent<EnemyClass>().IsDead) return;
		if (Random.value <= MainGameSingletone.Instance.PlayerStats.PlayerEvasion 
			&& collision.GetComponent<EnemyProjectileScript>()) {
			_hudText.Add(Localization.Get ("dodge"), Color.green, 0f);
        }
        else if (collision.GetComponent<EnemyClass>() != null && collision.GetComponent<EnemyClass>().IsDead)
            return;
        else
            Death();
    }

    private void Death()
    {
        MasterAudio.PlaySound(DeadSound);

        if (MainGameSingletone.Instance.PlayerStats.PlayerHealthPoints.Equals(0)) {
// temporall stub. should show defeat screen
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
        else {
			MainGameSingletone.Instance.PlayerStats.AddPointsToScore(-MainGameSingletone.Instance.PointsForDeath);
			MainGameSingletone.Instance.BuffSystem.ClearBuffs();
			MainGameSingletone.Instance.PlayerStats.RemoveHealthPoints(1);
            ResetPlayerPosition();
            StartCoroutine(Blink());
        }
    }

    protected virtual void Skill()
    {}

    protected virtual void SuperSkill()
    {}

    protected virtual void SuperEffect()
    {}

    private IEnumerator Blink()
    {
        _isBlinking = true;
        var alpha = 0.6f;
        for (var i = 0; i < 8; i++) {
            _rend.color = new Color(_rend.color.r, _rend.color.g, _rend.color.b, alpha);
            alpha = alpha.Equals(1f) ? 0.6f : 1f;
            yield return new WaitForSeconds(0.25f);
        }
        MainGameSingletone.Instance.PlayerStats.IsInvincible = false;
        _isBlinking = false;
    }

    private IEnumerator Fire()
    {
        var projectile = Resources.Load(ProjectilePrefabPath);
        float xPos;
        switch (MainGameSingletone.Instance.PlayerStats.PlayerProjectileCount) {
            case 1:
                xPos = 0.2f;
                break;
            case 2:
                xPos = 0.1f;
                break;
            case 3:
                xPos = 0f;
                break;
            default:
                xPos = 0f;
                break;
        }
        for (var i = 0; i < MainGameSingletone.Instance.PlayerStats.PlayerProjectileCount; i++) {
            var go = Instantiate(projectile) as GameObject;
            if (go == null) continue;
            MainGameSingletone.Instance.PlayerStats.CurrentShotsAllNumber++;
            var yPos = MainGameSingletone.Instance.PlayerStats.PlayerProjectileCount == 3 && i != 1
                ? transform.position.y - 0.1f
                : transform.position.y;
            go.transform.position = new Vector3(transform.position.x + xPos, yPos);
            go.transform.localScale = Vector3.one;
            xPos += 0.2f;
        }
        yield return null;
    }

    private void ResetPlayerPosition()
    {
        gameObject.transform.localPosition = new Vector3(0.0f, -1.05f, 0.0f);
    }

    public void Init()
    {
        if (_skillEventDelegate == null)
            _skillEventDelegate = new EventDelegate(Skill);
        _skill.onClick.Add(_skillEventDelegate);

        if (_superSkillEventDelegate == null)
            _superSkillEventDelegate = new EventDelegate(SuperSkill);
        _superSkill.onClick.Add(_superSkillEventDelegate);

        MainGameSingletone.Instance.PlayerStats.InvincibilityChanged += OnInvincibilityChanged;
        MainGameSingletone.Instance.StateManager.StateChangedEvent += OnStateChanged;
    }

    private void OnInvincibilityChanged(bool state)
    {
        _bubble.SetActive(state);
    }

    private void OnStateChanged(GameState prevState, GameState newState)
    {
        if (newState == GameState.NotStarted) {
            StopAllCoroutines();
            ResetPlayerPosition();
            _rend.color = Color.white;
            _isBlinking = false;
            gameObject.SetActive(false);
        }
        else {
            gameObject.SetActive(true);
        }
    }

    public void Uninit()
    {
        MainGameSingletone.Instance.PlayerStats.InvincibilityChanged -= OnInvincibilityChanged;
        MainGameSingletone.Instance.StateManager.StateChangedEvent -= OnStateChanged;
    }
}
