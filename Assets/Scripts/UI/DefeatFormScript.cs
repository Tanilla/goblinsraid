﻿using UnityEngine;
using System.Collections;

public class DefeatFormScript : MonoBehaviour 
{
	[SerializeField] private UIButton _nextButton;
	[SerializeField] private GameObject _statisticsScreen;
	[SerializeField] private UILabel _finalScoreLabel = null;

	private EventDelegate _nextButtonEventDelegate;

	private void Start()
	{
		if (_finalScoreLabel != null)
			_finalScoreLabel.text = MainGameSingletone.Instance.UIManager.ScoreWidget._scoreLabel.text;
	}

	private void OnEnable()
	{
		if (_nextButtonEventDelegate == null)
			_nextButtonEventDelegate = new EventDelegate(OnNextButtonReleased);
		_nextButton.onClick.Add(_nextButtonEventDelegate);

		MainGameSingletone.Instance.WaveManager.showVictoryScreen = true;
	}

	private void OnNextButtonReleased()
	{
		_statisticsScreen.gameObject.SetActive (true);
		MainGameSingletone.Instance.StateManager.SetNotInGame();
		gameObject.SetActive(false);
	}

	private void OnDisable()
	{
		_nextButton.onClick.Remove(_nextButtonEventDelegate);
	}
}
