using UnityEngine;

public class AttackState : StateMachineBehaviour
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponentInParent<EnemyClass>().Invoke("Shoot", animator.GetComponentInParent<EnemyClass>().InvokeDelay);
    }
}
